#!/bin/bash

set -u

#######################################
#             PARAMETERS              #
#######################################

NUM_ITERATIONS=10

SERVER=http://127.0.0.1:8080/api

APP_NAME=org.my-co.my-todo

#######################################
#         UTILITY PROCEDURES          #
#######################################

function prepare_payloads() {
  local VERSION=$1

  CREATE_PAYLOAD="{\"id\":\"$APP_NAME\",\"version\":\"$VERSION\",\"type\":\"brooklyn\",\"definition\":$BLUEPRINT}"
  DEPLOY_PAYLOAD="{\"blueprintId\":\"$APP_NAME\",\"blueprintVersion\":\"$VERSION\",\"location\":\"$LOCATION\"}"
}

function login() {
  local URL="$SERVER$1"
  local USER=$2
  local PASS=$3
  curl -v -c $COOKIE_FILE --data-binary "{\"user\":\"$USER\", \"password\":\"$PASS\"}" --header "Content-Type: application/json" $URL
  sleep $REST_TIME
}

function do_post() {
  local URL="$SERVER$1"
  local PAYLOAD="$2"
  curl -v -b $COOKIE_FILE --data-binary "$PAYLOAD" --header "Content-Type: application/json" $URL
  sleep $REST_TIME
}

function do_delete() {
  local URL="$SERVER$1"
  curl -v -b $COOKIE_FILE -X DELETE --header "Content-Type: application/json" $URL
  sleep $REST_TIME
}

function do_get() {
  local URL="$SERVER$1"
  curl -v -b $COOKIE_FILE --header "Content-Type: application/json" $URL
  sleep $REST_TIME
}

function get_deployment_id() {
  local JSON=$1
    echo "$JSON" | grep -o 'id"\s*:\s*"[^"]*"' | grep -v $APP_NAME | grep -o '"[^"]*"$' | grep -o '[^"].*[^"]'
}

function t_login() {
  login /login $USERNAME $PASSWORD
}

function t_deploy() {
  VERSION=$BASE_VERSION
  for i in `seq 1 $NUM_ITERATIONS`;
  do
    prepare_payloads $VERSION
    do_post /blueprints "$CREATE_PAYLOAD"
    do_post /deployments "$DEPLOY_PAYLOAD"
    ((VERSION++))
  done
}

function t_delete {
  VERSION=$BASE_VERSION
  for i in `seq 1 $NUM_ITERATIONS`;
  do
    JSON=$(do_get "/blueprints/$APP_NAME/$VERSION")
    DEP_ID=$(get_deployment_id "$JSON")

    if [[ -n "$DEP_ID" ]] ; then
      do_delete "/deployments/$DEP_ID"
    fi
    do_delete "/blueprints/$APP_NAME/$VERSION"
    ((VERSION++))
  done
}

function usage() {
  cat << EOF
  usage: $0 options

  Execute test commands over a CIM rest server

  OPTIONS/COMMANDS:
     -s S   Sleep S seconds between commands (default $SLEEP_TIME)
     -r R   Sleep R seconds after each API call (default $REST_TIME)
     -n N   Perform N iterations (default 10)
     -l     Login to server
     -u U   Use username U to login (default $USERNAME)
     -p P   Use password P to login (default $PASSWORD)
     -t     Submit and deploy blueprints
     -d     Delete blueprints
     -b B   Start from version B (default $BASE_VERSION)
     -L L   Deploy to location L (default $LOCATION)
     -B B   Use blueprint file B (default $BLUEPRINT_FILE)
     -C C   Use cookie jar file C (default new mktemp file)

  EXAMPLES:
  $0 -n 20 -l -t -C cookies.txt
  Login; Submit and deploy 20 blueprints

  $0 -n 20 -d -C cookies.txt
  Delete 20 blueprints in the same session as previous command
EOF
}

#######################################
#           MAIN PROCEDURE            #
#######################################

### Default values

BASE_VERSION=1
O_LOGIN=
O_DEPLOY=
O_DELETE=
SLEEP_TIME=30
REST_TIME=5
LOCATION="default-location"
BLUEPRINT_FILE="./sample.txt"
COOKIE_FILE=
USERNAME=test
PASSWORD=test

if [[ $# -eq 0 ]] ; then
  usage
  exit 0
fi

### Parse parameters ###

while getopts "s:r:n:lu:p:tdb:L:B:C:" OPTION
do
  case $OPTION in
    s)
      SLEEP_TIME=$OPTARG
      ;;
    r)
      REST_TIME=$OPTARG
      ;;
    n)
      NUM_ITERATIONS=$OPTARG
      ;;
    l)
      O_LOGIN=1
      ;;
    u)
      USERNAME="$OPTARG"
      ;;
    p)
      PASSWORD="$OPTARG"
      ;;
    t)
      O_DEPLOY=1
      ;;
    d)
      O_DELETE=1
      ;;
    b)
      BASE_VERSION=$OPTARG
      ;;
    L)
      LOCATION="$OPTARG"
      ;;
    B)
      BLUEPRINT_FILE="$OPTARG"
      ;;
    C)
      COOKIE_FILE="$OPTARG"
      ;;
    ?)
      usage
      exit 1
      ;;
    esac
done

### Execute commands ###

if ! [[ -f $BLUEPRINT_FILE ]] ; then
  echo File not found: $BLUEPRINT_FILE
  exit 1
fi
BLUEPRINT=$(python -c "import sys; import json; print json.dumps(sys.stdin.read());" < $BLUEPRINT_FILE)

if [[ -z $COOKIE_FILE ]] ; then
  COOKIE_FILE=$(mktemp "$TMPDIR/CookiesXXXXXX")
fi

if [[ -n $O_LOGIN ]] ; then
  echo Logging in
  t_login
fi

if [[ -n $O_DEPLOY ]] ; then
  echo Creating deployments
  t_deploy
  if [[ -n $O_DELETE ]] ; then
    echo Waiting $SLEEP_TIME seconds...
    sleep $SLEEP_TIME
  fi
fi

if [[ -n $O_DELETE ]] ; then
  echo Deleting deployments
  t_delete
fi
